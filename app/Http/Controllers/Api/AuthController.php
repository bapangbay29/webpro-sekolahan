<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api',['except' => ['login','register']]);
    }

    public function login (Request $request)
    {
        $penyaringan = Validator::make($request ->all(),[
        'username' => 'required',
        'password' => 'required',
    ]);

    if ($penyaringan->fails()){
        $message = $penyaringan->errors();

        return $this -> failedResponse ($message,422);
    }

    $credentials = request(['username', 'password']);
   
    // if (auth()->attempt($credentials)) {
    //     dd('benar');
    // } else {
    //     dd('salah');
    // }//tambahan
    
    if (!$token = auth()->attempt($credentials)){
        return $this -> failedResponse ('Username atau password salah!',401);
    }

    return response()->json([
        'status' => true,
        'message' => 'Logged in',
        'token' => $token
    ]);
    }
    private function success($data,$statusCode,$message='success'){
        return response()->json([
            'status' => true,
            'message' => $message,
            'data' => null,
            'status_code' => $statusCode
        ],$statusCode);
    }
    private function failedResponse($message, $statusCode){
        return response()->json([
            'status' => false,
            'message' => $message,
            'data' => null,
            'status_code' => $statusCode
        ],$statusCode);
    }
}
