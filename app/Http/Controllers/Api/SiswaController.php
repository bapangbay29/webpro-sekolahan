<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\Siswa;
use Illuminate\Http\Request;

class SiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Siswa::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $penyaringan = Validator::make($request ->all(),[
            'user_id' => 'required|exists:users,id',
            'nama' => 'required|string',
            'tempat_lahir' => 'required|string',
            'tgl_lahir' => 'required|string',
            'gender' => 'required|IN:laki-laki,perempuan',
            'phone_number' => 'nullable|string',
            'email' => 'required|string|unique:siswa,email',
            'alamat' => 'nullable|string',
        ]);
        if ($penyaringan->fails()){
            $pesan = $penyaringan->errors();

            return $this -> failedResponse ($pesan,422);
        }
        $siswa = new Siswa();
        $siswa->user_id = $request->user_id;
        $siswa->nama = $request->nama;
        $siswa->tempat_lahir = $request->tempat_lahir;
        $siswa->tgl_lahir = $request->tgl_lahir;
        $siswa->gender = $request->gender;
        $siswa->phone_number = $request->phone_number;
        $siswa->email = $request->email;
        $siswa->alamat = $request->alamat;
        $tambahSiswa = $siswa->save();

        if ($tambahSiswa){
            return $this->success($siswa,201);
        }else{
            return $this->failedResponse('Siswa gagal ditambahkan',500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function show(Siswa $siswa)
    {
        return $this->success($siswa,200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Siswa $siswa)
    {
        $penyaringan = Validator::make($request ->all(),[
            'user_id' => 'required|exists:users,id',
            'nama' => 'required|string',
            'tempat_lahir' => 'required|string',
            'tgl_lahir' => 'required|string',
            'gender' => 'required|IN:laki-laki,perempuan',
            'phone_number' => 'nullable|string',
            'email' => 'required|string|unique:siswa,email',
            'alamat' => 'nullable|string',
        ]);
        if ($penyaringan->fails()){
            $message = $penyaringan->errors();

            return $this -> failedResponse ($message,422);
        }
        
        $siswa->user_id = $request->user_id;
        $siswa->nama = $request->nama;
        $siswa->tempat_lahir = $request->tempat_lahir;
        $siswa->tgl_lahir = $request->tgl_lahir;
        $siswa->gender = $request->gender;
        $siswa->phone_number = $request->phone_number;
        $siswa->email = $request->email;
        $siswa->alamat = $request->alamat;
        $tambahSiswa = $siswa->save();

        if ($saved){
            return $this -> success ($siswa,200);
        }else{
            return $this-> failedResponse('User Gagal Update',500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function destroy(Siswa $siswa)
    {
         if ($deletData){
            return $this ->success(null,200);

        }else {
            return $this -> failedResponse('User Gagal Hapus',500 );
        }
    }
    private function success($data,$statusCode,$message='success')
    {
        return response()->json([
        'status' => true,
        'message' => $message,
        'data' => $data,
        'status_code' => $statusCode
        ],$statusCode);
    }

    private function failedResponse($message,$statusCode)
     {
        return response()->json([
        'status' => false,
        'message' => $message,
        'data' => null,
        'status_code' => $statusCode
        ],$statusCode);
     }
}
