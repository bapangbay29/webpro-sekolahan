<?php

use Illuminate\Database\Seeder;
use App\User;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'type' => 'admin',
            'username' => ' kamu',
            'password' =>Hash::make('qwerty')
        ]);
        User::create([
            'type' => 'guru',
            'username' => ' aku',
            'password' =>Hash::make('qwerty')
        ]);
        User::create([
            'type' => 'admin',
            'username' => ' bapang',
            'password' =>Hash::make('qwerty')
        ]);
        User::create([
            'type' => 'admin',
            'username' => ' bayu',
            'password' =>Hash::make('qwerty')
        ]);
        User::create([
            'type' => 'admin',
            'username' => ' pangestu',
            'password' =>Hash::make('qwerty')
        ]);
        
    }
}
